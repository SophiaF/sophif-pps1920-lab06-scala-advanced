package u06lab.code

/**
  * 1) Implement trait Functions with an object FunctionsImpl such that the code
  * in TryFunctions works correctly.
  *
  * 2) To apply DRY principle at the best,
  * note the three methods in Functions do something similar.
  * Use the following approach:
  * - find three implementations of Combiner that tell (for sum,concat and max) how
  *   to combine two elements, and what to return when the input list is empty
  * - implement in FunctionsImpl a single method combiner that, other than
  *   the collection of A, takes a Combiner as input
  * - implement the three methods by simply calling combiner
  *
  * When all works, note we completely avoided duplications..
 */

trait Functions {
  def union[A](a: Seq[A])(fun: (A, A) => A ): A
  def sum(a: List[Double]): Double
  def concat(a: Seq[String]): String
  def max(a: List[Int]): Int // gives Int.MinValue if a is empty
}

trait Combiner[A] {
  def unit: A
  def combine(a: A, b: A): A
}


object FunctionsImpl extends Functions {

  override def union[A](a: Seq[A])(fun: (A, A) => A ): A = a match {
    case h :: t => fun(h, union(t)(fun))
    case h :: _ => h
  }

  override def sum(a: List[Double]): Double = a match{
    case h :: t => h+sum(t)
    case _ => 0
  }

  override def concat(a: Seq[String]): String = a match {
    case h :: t => h+concat(t)
    case _ => ""
  }

  override def max(a: List[Int]): Int = a match {
    case h :: t if h > max(t) => h
    case h :: t => max(t)
    case _ => Integer.MIN_VALUE
  }





}

object TryFunctions extends App {
  val f: Functions = FunctionsImpl
  println(f.sum(List(10.0,20.0,30.1))) // 60.1
  println(f.sum(List()))                // 0.0
  println(f.concat(Seq("a","b","c")))   // abc
  println(f.concat(Seq()))              // ""
  println(f.max(List(-10,3,-5,0)))      // 3
  println(f.max(List()))                // -2147483648
}